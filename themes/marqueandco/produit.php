<?php
/*
  Template Name: Produits
*/
  	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
	}

	get_header();
?>
<main>

        <section id="section4" class="projet">
            <div class="container">
                <div class="row projets">
					<?php
						$arg = array(
							'post_type' => 'projet',
						);
						$q = new WP_Query( $arg );
						if( $q->have_posts() ):
							while( $q->have_posts() ):
								$q->the_post();

								$class = get_the_title();
								if( stripos( 'custom', $class ) ) $class = 'item_custom';
					?>

                    <div class="col-md-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                        <a href="#">
                            <div class="card">
                                <img src="<?= get_field('projet_image') ?>" alt="">
                                <div class="card-body">
                                <h4 class="card-title"><?= get_field('projet_titre') ?></h4>
                                <p class="card-text"><?= get_field('projet_description') ?></p>
                                <span class="btnAdd">+</span>
                                </div>
                            </div>
                        </a>
                    </div>
					<?php
					endwhile;
						wp_reset_postdata();
					endif;
					?>
                    
                </div>
            </div>
        </section>

    </main>

<?php

	get_footer();
?>