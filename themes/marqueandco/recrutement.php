<?php
/*
  Template Name: Recrutement
*/
  	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
	}

	get_header();
?>
<main>

        <section id="section18" class="blocJob">
            <div class="container">
                <div class="row d-flex justify-content-center align-items-center">
                <div id="accordion" class="col">
                    <h2>
                        <span class="introAccord">Création</span>
                        Stagiaire graphiste
                        <span  class="concAccord">Stage H/F</span>
                    </h2>
                    <div>
                        <div class="sousAccordion">
                            <div class="accordTitre d-flex justify-content-start align-items-start">
                                <h3>Infos <br>générales</h3>
                                <div class="sousAccord d-flex justify-content-start align-items-start">
                                    <div>
                                        <h4>Contrat</h4>
                                        <p>Stage conventionné</p>
                                    </div>
                                    <div>
                                        <h4>Lieu</h4>
                                        <p>Marque&Co</p>
                                        <p>Reuil Malmaison</p>
                                    </div>
                                    <div>
                                        <h4>Durée</h4>
                                        <p>Pour une durée minimum de 6 mois, à partir de juillet 2021</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordDetail d-flex justify-content-start align-items-start">
                                <h3>Détails</h3>
                                <div class="sousAccord">
                                    <h4>Description de poste</h4>
                                    <p>Vous recherchez un stage significatif et enrichissant au sein d’une agence de communication ? Marque and Co est à la recherche de son stagiaire Graphiste pour accompagner les équipes sur des projets structurants. Dans le cadre de ce stage, vous serez en charge de réaliser des créations à destination de nos clients issus de secteurs très variés.</p>
                                    <h4>Vos missions</h4>
                                    <ul>
                                        <li>- Travailler avec les équipes projets dans un univers diversifié et très talentueux <br>
                                        Décliner des concepts créatifs et créer les vôtres</li>
                                        <li>- Illustrer, retoucher, animer, mettre en avant l’information pour la rendre la plus impactante possible</li>
                                        <li>- Assister aux briefs clients <br>
                                        Vous travaillerez dans un environnement dynamique sur des projets stimulants. Vous aurez, en plus de vos compétences professionnelles, un vrai goût pour le challenge et un esprit très créatif.
                                        </li>
                                    </ul>
                                    <h4>Profil recherché</h4>
                                    <ul>
                                        <li>- Vous êtes en dernière année d’étude</li>
                                        <li>- Vous maitrisez la suite Adobe et principalement les logiciels Photoshop, Illustrator et InDesign. La maitrise d’After Effect et le montage vidéo serait un vrai plus</li>
                                        <li>- Vous êtes sensible à la culture des réseaux sociaux</li>
                                        <li>- Vous êtes créatif et vous savez vous adapter à différents univers</li>
                                        <li>- Vous êtes rigoureux, organisé et capable de travailler en autonomie</li>
                                    </ul>
                                    <h4>Les avantages</h4>
                                    <ul>
                                        <li>- Locaux hyper-spacieux, situés à Rueil Malmaison</li>
                                        <li>- Salle de sport privative (Bord de Seine)</li>
                                        <li>- Gratification de 1000 € brut</li>
                                        <li>- Tickets restaurant</li>
                                        <li>- Possibilité d’embauche à la fin du stage</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="link">
                                <span>Vous pensez avoir le profil ? </span>
                                <a href="#" class="btn">Répondre à l’offre</a>
                            </div>
                        </div>
                    </div>
                    <h2>
                        <span class="introAccord">Création</span>
                        Stagiaire graphiste
                        <span  class="concAccord">Stage H/F</span>
                    </h2>
                    <div>
                        <div class="sousAccordion">
                            <div class="accordTitre d-flex justify-content-start align-items-start">
                                <h3>Infos <br>générales</h3>
                                <div class="sousAccord d-flex justify-content-start align-items-start">
                                    <div>
                                        <h4>Contrat</h4>
                                        <p>Stage conventionné</p>
                                    </div>
                                    <div>
                                        <h4>Lieu</h4>
                                        <p>Marque&Co</p>
                                        <p>Reuil Malmaison</p>
                                    </div>
                                    <div>
                                        <h4>Durée</h4>
                                        <p>Pour une durée minimum de 6 mois, à partir de juillet 2021</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordDetail d-flex justify-content-start align-items-start">
                                <h3>Détails</h3>
                                <div class="sousAccord">
                                    <h4>Description de poste</h4>
                                    <p>Vous recherchez un stage significatif et enrichissant au sein d’une agence de communication ? Marque and Co est à la recherche de son stagiaire Graphiste pour accompagner les équipes sur des projets structurants. Dans le cadre de ce stage, vous serez en charge de réaliser des créations à destination de nos clients issus de secteurs très variés.</p>
                                    <h4>Vos missions</h4>
                                    <ul>
                                        <li>- Travailler avec les équipes projets dans un univers diversifié et très talentueux <br>
                                        Décliner des concepts créatifs et créer les vôtres</li>
                                        <li>- Illustrer, retoucher, animer, mettre en avant l’information pour la rendre la plus impactante possible</li>
                                        <li>- Assister aux briefs clients <br>
                                        Vous travaillerez dans un environnement dynamique sur des projets stimulants. Vous aurez, en plus de vos compétences professionnelles, un vrai goût pour le challenge et un esprit très créatif.
                                        </li>
                                    </ul>
                                    <h4>Profil recherché</h4>
                                    <ul>
                                        <li>- Vous êtes en dernière année d’étude</li>
                                        <li>- Vous maitrisez la suite Adobe et principalement les logiciels Photoshop, Illustrator et InDesign. La maitrise d’After Effect et le montage vidéo serait un vrai plus</li>
                                        <li>- Vous êtes sensible à la culture des réseaux sociaux</li>
                                        <li>- Vous êtes créatif et vous savez vous adapter à différents univers</li>
                                        <li>- Vous êtes rigoureux, organisé et capable de travailler en autonomie</li>
                                    </ul>
                                    <h4>Les avantages</h4>
                                    <ul>
                                        <li>- Locaux hyper-spacieux, situés à Rueil Malmaison</li>
                                        <li>- Salle de sport privative (Bord de Seine)</li>
                                        <li>- Gratification de 1000 € brut</li>
                                        <li>- Tickets restaurant</li>
                                        <li>- Possibilité d’embauche à la fin du stage</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="link">
                                <span>Vous pensez avoir le profil ? </span>
                                <a href="#" class="btn">Répondre à l’offre</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

<?php

	get_footer();
?>