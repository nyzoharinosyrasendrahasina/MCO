<?php
/*
  Template Name: Homepage
*/
  	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
	}

	get_header();
?>
<main>       
		<section id="section1" class="d-flex justify-content-center align-items-center">
			<div class="container d-flex justify-content-between align-items-center">
				<div class="blocImg">
					<div class="imgBig wow fadeInUp">
						<img src="<?= get_field("agence_big") ?>" alt="">
					</div>
					<div class="imgSmall wow slideInLeft" data-wow-duration="1s" data-wow-delay="1s">
						<img src="<?= get_field("agence_small") ?>" alt="">
					</div>
					<div class="imgVerySmall wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay="1.2s">
						<img src="<?= get_field("agence_very_small") ?>" alt="">
					</div>
				</div>
				<div class="blocText wow fadeInRight">
				<div class="blocTitre">
					<span><?= get_field("agence_titre1") ?></span>
					<h2><?= get_field("agence_titre2") ?></h2>
					</div><p><?= get_field("agence_description") ?></p><a href="/agence" class="btn" title="En savoir plus">En savoir plus</a></div>
			</div>
		</section>
	
        <section id="section2" class="d-flex justify-content-center align-items-center">
            <div class="container">
                <div class="row introText">
                    <div class="col text-center wow fadeInUp" data-wow-duration="600ms" data-wow-delay="600ms">
                        <p><?= get_field("expertise_grand_titre") ?></p>
                    </div>
                </div>
            </div>
        </section>
		<?php  
			$liste_expertise = get_field('liste_expertise', get_the_ID());
			if(isset($liste_expertise)):
		?>
        <section id="section3" class="d-flex justify-content-center align-items-center">
            <div class="container">
                <div class="row">					
					<?php if(!empty($liste_expertise)):
						foreach ($liste_expertise as $key => $expertise) :
					?>
						<div class="col-lg-4 col-md-12 textExpertise wow slideInUp">						
							<div class="img-expertise img-expertise<?= ++$key ?>">
								<img src="<?= $expertise['image']; ?>" alt="">
							</div>
							<div class="expertise">
								<h3><?= $expertise['titre']; ?></h3>
								<p><?= $expertise['sous-titres']; ?></p>
								<p><?= $expertise['description']; ?></p>
							</div>
						</div> 
					<?php endforeach; endif; ?>
                </div>
            </div>
        </section>
		<?php
			endif;
		?>
		<section id="section4">
            <div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-12 d-flex justify-content-lg-start justify-content-center wow fadeInLeft">
						<p>Projets</p>
					</div>
					<div class="col-lg-6 col-md-12 d-flex justify-content-lg-end justify-content-center wow fadeInRight">
						<a href="/projets" class="btn" title="En savoir plus">Tout voir</a>
					</div>
				</div>
				<div class="row projets">
					<?= section_projet(6) ?>
				</div>
				
				<div class="text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
					<a href="/projets" class="btn">En savoir plus</a>
				</div>
				
			</div>
        </section>
		
		<?= section_partenaire() ?>
        
        <section id="section6" class="vh d-flex justify-content-center align-items-center" style="background: url('<?= get_field('section_contact_image_fond') ?>') center bottom no-repeat !important; background-size: cover !important; padding: 93px 0 205px !important;">
            <div class="container">
                <div class="row">
                    <div class="col hello wow fadeInUp">
                        <div class="blocTitre">
                            <span><?= get_field('section_contact_titre_1') ?></span>
                            <h2><?= get_field('section_contact_titre_2') ?></h2>
                        </div>
                        <ul>
                            <li>
                                <span><?= get_field('section_contact_texte_email_1') ?></span>
                                <a href="mailto:contact@marqueandco.fr"><?= get_field('section_contact_email_1') ?></a>
                            </li>
                            <li>
                                <span><?= get_field('section_contact_texte_email_2') ?></span>
                                <a href="mailto:presse@marqueandco.fr"><?= get_field('section_contact_email_2') ?></a>
                            </li>
                            <li>
                                <span><?= get_field('section_contact_texte_email_3') ?></span>
                                <a href="mailto:job@marqueandco.fr"><?= get_field('section_contact_email_3') ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>		
		<?= liste_marquee() ?>
        <section id="section8" class="vh d-flex justify-content-center align-items-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="blocTitre text-center">
                            <h3>Nous contacter</h3>
                        </div>
						
						<?php echo do_shortcode( '[contact-form-7 id="86" title="Formulaire de contact"]' ); ?>
                        
                    </div>
                </div>
            </div>
        </section>
    </main>

<?php

	get_footer();
?>