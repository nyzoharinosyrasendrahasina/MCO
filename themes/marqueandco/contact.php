<?php
/*
  Template Name: Contact
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();
?>
<main>

    <section id="section11" class="blocCoordonner">
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="map wow fadeInLeft">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2623.778135045134!2d2.1616743158699436!3d48.88150590703244!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66315a6d7b6f1%3A0xb984aa20a2c1fca4!2s<?= urlencode(get_field("adresse", 'option')) ?>!5e0!3m2!1sfr!2smg!4v1626096852829!5m2!1sfr!2smg" width="425" height="425" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    <div class="blocRsContact">
                        <a href="<?= get_field("linkedin", 'option') ?>" class="rsContact linkedin"></a>
                        <a href="<?= get_field("facebook", 'option') ?>" class="rsContact facebook"></a>
                        <a href="<?= get_field("instagram", 'option') ?>" class="rsContact instagram"></a>
                    </div>
                </div>
                <div class="textCoor wow fadeInRight">
                    <div class="blocTitre">
                        <span>Contact</span>
                        <h2>Marque&Co</h2>
                    </div>
                    <p><?= get_field("adresse", 'option') ?></p>
                    <p><a href="<?= get_field("tel", 'option') ?>"><?= get_field("tel", 'option') ?></a></p>
                </div>
            </div>
        </div>
    </section>

    <section id="section8" class="blocContact vh d-flex justify-content-center align-items-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-12 wow fadeInUp">
                    <div class="blocTitre text-center">
                        <h3>Nous contacter</h3>
                    </div>

                    <?php echo do_shortcode('[contact-form-7 id="86" title="Formulaire de contact"]'); ?>

                </div>
            </div>
        </div>
    </section>

</main>

<?php

get_footer();
?>