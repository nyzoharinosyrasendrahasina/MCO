<?php
/**
 * marqueandco Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package marqueandco
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_MARQUEANDCO_VERSION', '1.0.0' );
define( 'ASTRA_THEME_DIR', trailingslashit( get_template_directory() ) );
define( 'ASTRA_THEME_URI', trailingslashit( esc_url( get_template_directory_uri() ) ) );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'marqueandco-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_MARQUEANDCO_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

// Modèles projet
//require_once ASTRA_THEME_DIR . 'inc/modele-projet.php';

/* css & js */
define('CSS_URL', get_template_directory_uri() . '/assets/css/');
define('JS_URL', get_template_directory_uri() . '/assets/js/');
define('IMG_URL', get_template_directory_uri() . '/assets/images/');
define('VIDEOS_URL', get_template_directory_uri() . '/assets/videos/');
define('THEME_URL', get_template_directory_uri() . '/');
define('WHISE_JS_URL', get_template_directory_uri() . '/whise/js/');

// Modèles projet
require_once ASTRA_THEME_DIR . 'inc/modele-projet.php';
require_once ASTRA_THEME_DIR . 'inc/cpt-partenaire.php';
require_once ASTRA_THEME_DIR . 'inc/cpt-marquee.php';

function load_css_js_front() {

	wp_register_script( 'jquery', JS_URL . 'jquery.min.js', array(), true, false, true );
	wp_register_script( 'jquery-ui', JS_URL . 'jquery-ui.min.js', array(), true, false, true );
	wp_register_script( 'slick', JS_URL . 'slick.min.js', array(), true, false, false );
	wp_register_script( 'wow', JS_URL . 'wow.js', array(), true, false, false );
	wp_register_script( 'custom', JS_URL . 'custom.js', array(), null, false, false );
	wp_enqueue_style( 'fancybox', JS_URL.'fancybox/jquery.fancybox.css', array(), false );

	wp_enqueue_style( 'jquery-ui', CSS_URL . 'jquery-ui.min.css' );
	wp_enqueue_style( 'slick', CSS_URL . 'slick.css' );
	wp_enqueue_style( 'animate', CSS_URL . 'animate.css' );
	wp_enqueue_style( 'bootstrap', CSS_URL . 'bootstrap.min.css' );
	wp_enqueue_style( 'styles', CSS_URL . 'styles.css' );

	wp_enqueue_script( 'jquery', false, array(), false, true );
	wp_enqueue_script( 'jquery-ui', false, array(), false, true );
	wp_enqueue_script('fancybox/jquery.fancybox', JS_URL.'fancybox/jquery.fancybox.js',  array(), true, true );
	wp_enqueue_script( 'slick', false, array(), null, true );
    wp_enqueue_script( 'isotop', false, array(), false, true );
    wp_enqueue_script( 'wow', false, array(), null, true );
	wp_enqueue_script( 'custom', false, array(), null, true );

}
add_action( 'wp_enqueue_scripts', 'load_css_js_front' );

function add_menuclass($ulclass) {
    return preg_replace('/rel="data-fancybox"/', 'data-fancybox', $ulclass, -1);
}
add_filter('wp_nav_menu','add_menuclass');

//Ajouter menu Admin
add_theme_support("nav_menu");
register_nav_menu('Main-Menu','Navigation principal');
register_nav_menu('Burger-Menu-Left','Navigation burger gauche');
register_nav_menu('Burger-Menu-Right','Navigation burger droite');
//register_nav_menu('Footer-Menu-Left','Menu de pied de page');

function rand_array($array) {	
    srand((float)microtime()*1000000);
    shuffle($array);
	return $array; 
}

function liste_marquee() {
	$html = '<section id="section7">
            <marquee behavior="scroll" direction="left" width="100%">';				
				$arg = array(
					'post_type' => 'marquee',
					'posts_per_page' => -1
				);
				$q = new WP_Query( $arg );
				if( $q->have_posts() ):
						while( $q->have_posts() ):
							$q->the_post();

							$class = get_the_title();
							if( stripos( 'custom', $class ) ) $class = 'item_custom';					
							
							$html .= '<a href="' .get_field('marquee_url').'">Marque&Co '.get_field('marquee_titre').'</a>';							
					
						endwhile;
					wp_reset_postdata();
				endif;
						
        $html .= '</marquee>
        </section>';		
	
		
	return $html;
}

function section_projet($limit) {
	$html = '';
	$arg = array(
		'post_type' => 'projet',
		'posts_per_page' => $limit
	);
	$q = new WP_Query( $arg );
	if( $q->have_posts() ):
		while( $q->have_posts() ):
			$q->the_post();

			$class = get_the_title();
			if( stripos( 'custom', $class ) ) $class = 'item_custom';
						
			$html .= '<div class="col-md-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
						<a href="' . get_the_permalink() .'" class="item_img">
							<div class="card">
								<img src="'. get_field('projet_image') .'" alt="">
								<div class="card-body">
									<h4 class="card-title">' . get_field('projet_titre') . '</h4>
									<p class="card-text">' . get_field('projet_description') .'</p>
									<span class="btnAdd">+</span>
								</div>
                            </div>
                        </a>
                    </div>';
					
		endwhile; 
		wp_reset_postdata();
	endif;
			
	return $html;
}

function section_partenaire() {
	$html = '<section id="section5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12 d-flex justify-content-lg-start justify-content-center wow fadeInLeft">
                        <p>Ils ont déjà travaillé avec nous</p>
                    </div>
                    <div class="col-lg-6 col-md-12 d-flex justify-content-lg-end justify-content-center wow fadeInRight">
                        <a href="#" class="btn" title="En savoir plus">En savoir plus</a>
                    </div>
                </div>
                <div class="row slide-partenaire justify-content-around align-items-center">';
									
					
						$arg = array(
							'post_type' => 'partenaires',
							'posts_per_page' => -1
						);
						$q = new WP_Query( $arg );
						if( $q->have_posts() ):
							while( $q->have_posts() ):
								$q->the_post();

								$class = get_the_title();
								if( stripos( 'custom', $class ) ) $class = 'item_custom';
					
					
						$html .= '<div class="col-lg-3 col-12 text-center wow slideInUp">
							<a href="' . get_field('partenaire_url') . '"><img src="'. get_field('partenaire_logo').'" alt=""></a>
						</div>';
					
							endwhile; 
						wp_reset_postdata();
						endif;
					
                  
				
                    
                $html .= '</div>
            </div>
        </section>';
		
	return $html;
}


function posts_link_next_class($format){
     $format = str_replace('href=', 'id="nav-next" class="nav-detail" href=', $format);
     return $format;
}

function posts_link_prev_class($format){
     $format = str_replace('href=', 'id="nav-prev" class="nav-detail" href=', $format);
     return $format;
}

add_filter('next_post_link', 'posts_link_next_class');
add_filter('previous_post_link', 'posts_link_prev_class');


if( function_exists('acf_add_options_page') ) {

	// Premier menu d'options
	acf_add_options_page(array(
		'page_title'    => 'Options',
		'menu_title'    => 'Options',
		'menu_slug'     => 'options-generales',
		'capability'    => 'edit_posts',
		'redirect'      => true
	));

	acf_add_options_sub_page(array(
		'page_title'    => 'Options réseaux sociaux',
		'menu_title'    => 'Réseaux sociaux',
		'parent_slug'   => 'options-generales',
	));
	
	acf_add_options_sub_page(array(
		'page_title'    => 'Options contact',
		'menu_title'    => 'Contact',
		'parent_slug'   => 'options-generales',
	));

}
