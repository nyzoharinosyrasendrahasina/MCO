<?php
/*
  Template Name: Agence
*/
  	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
	}

	get_header();
?>
<main>
        <section id="section9">
            <div class="container">
                <div class="blocTextImg blocTextImg1 d-flex justify-content-between align-items-center">
                    <div class="blocText wow fadeInLeft">
                        <div class="blocTitre">
                            <span>L’agence</span>
                            <h2>Ce que nous <br>sommes</h2>
                        </div>
                        <p><Strong>La créativité se puise dans l’humain.</Strong> <br>
                        Nous sommes une agence de communication construite autour d’une équipe de talents créatifs : un groupe très uni et diversifié où les limites s’effacent au profit du génie collectif. De taille humaine, nous pouvons collaborer plus étroitement avec nos clients - et les uns avec les autres, de sorte que toute notre expertise et notre expérience tournent autour de chaque projet. Et parce que nous travaillons tous ensemble, nous pouvons toujours exécuter de grandes idées et des campagnes réussies. C’est ce qui nous anime. Et c’est ce qui nous distingue.</p>
                    </div>
                    <div class="blocImg wow fadeInRight">
                        <div class="imgBig">
                            <img src="<?= IMG_URL."img-big.jpg" ?>" alt="">
                        </div>
                        <div class="imgSmall wow slideInRight" data-wow-duration="800ms" data-wow-delay="1s">
                            <img src="<?= IMG_URL."img-small.jpg" ?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="blocImgText blocTextImg2 d-flex justify-content-between align-items-center">
                    <div class="blocImg wow fadeInLeft">
                        <div class="imgBig">
                            <img src="<?= IMG_URL."img-big.jpg" ?>" alt="">
                        </div>
                        <div class="imgSmall wow fadeInLeft" data-wow-duration="800ms" data-wow-delay="1s">
                            <img src="<?= IMG_URL."img-small.jpg" ?>" alt="">
                        </div>
                        <div class="imgVerySmall wow fadeInLeft" data-wow-duration="800ms" data-wow-delay="1.2s">
                            <img src="<?= IMG_URL."img-very-small.jpg" ?>" alt="">
                        </div>
                    </div>
                    <div class="blocText wow fadeInRight">
                        <div class="blocTitre">
                            <span>L’agence</span>
                            <h2>Ce que nous <br>faisons</h2>
                        </div>
                        <p><Strong>Parce que chaque marque est unique ; chacune de nos recommandations l’est aussi.</Strong> <br>
                        Parce que chaque marque est unique, chaque projet offre un nouveau terrain de découverte. Explorer son histoire, décortiquer son marché, analyser ses cibles, définir ses insights et comprendre ses objectifs afin d’y donner un sens et faire émerger la marque dans un univers toujours plus concurrentiel. Chez Marque and Co, nous nous spécialisons dans l’activation des marques sur l’ensemble des canaux existants, de la création de marques de A à Z ou dans la redynamisation des marques existantes, en mettant l'accent sur la conception et la gestion de chacun des projets.</p>
                    </div>
                </div>
                <div class="blocTextImg blocTextImg3 d-flex justify-content-between align-items-center">
                    <div class="blocText wow fadeInLeft">
                        <div class="blocTitre">
                            <span>L’agence</span>
                            <h2>Ce que nous <br>croyons</h2>
                        </div>
                        <p><Strong>Parce que chaque marque est unique ; chacune de nos recommandations l’est aussi.</Strong> <br>
                        Parce que chaque marque est unique, chaque projet offre un nouveau terrain de découverte. Explorer son histoire, décortiquer son marché, analyser ses cibles, définir ses insights et comprendre ses objectifs afin d’y donner un sens et faire émerger la marque dans un univers toujours plus concurrentiel. Chez Marque and Co, nous nous spécialisons dans l’activation des marques sur l’ensemble des canaux existants, de la création de marques de A à Z ou dans la redynamisation des marques existantes, en mettant l'accent sur la conception et la gestion de chacun des projets.</p>
                    </div>
                    <div class="blocImg wow fadeInRight">
                        <div class="imgBig">
                            <img src="<?= IMG_URL."img-big.jpg" ?>" alt="">
                        </div>
                        <div class="imgVerySmall wow fadeInRight" data-wow-duration="800ms" data-wow-delay="1s">
                            <img src="<?= IMG_URL."img-very-small.jpg" ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
			$liste_partenaires = get_field('liste_partenaires', get_the_ID());
			if(isset($liste_partenaires)):
		?>
            <section id="section5">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-12 d-flex justify-content-center wow fadeInUp">
                            <p>Ils ont déjà travaillé avec nous</p>
                        </div>
                    </div>
                    <div class="row slide-partenaire justify-content-around align-items-center">
                        
                        <?php if(!empty($liste_partenaires)):
                            foreach ($liste_partenaires as $key => $partenaire) :
                        ?>
                        <div class="col-lg-3 col-12 text-center wow slideInUp">
                            <a href="<?= $partenaire['url']; ?>"><img src="<?= $partenaire['logo']; ?>" alt=""></a>
                        </div>
                        <?php endforeach; endif; ?>
                        
                    </div>
                </div>
            </section>
		<?php endif; ?>

        <section id="section10">
            <div class="container">
                <div class="slideTemoin">
                    <div class="itemTemoin">
                        “Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam.”
                        <div class="autheur">
                            Jean-Marie Boucheron
                            <strong>Dir. Com. Sanofi</strong>
                        </div>
                    </div>
                    <div class="itemTemoin">
                        “Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam.”
                        <div class="autheur">
                            Jean-Marie Boucheron
                            <strong>Dir. Com. Sanofi</strong>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
<?php

	get_footer();
?>