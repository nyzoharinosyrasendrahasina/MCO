<?php
/*
  Template Name: Green&CO
*/
  	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
	}

	get_header();
?>
<main>
					
		<section id="section1" class="blocIntroGreen d-flex justify-content-center align-items-center">
			<div class="container d-flex justify-content-between align-items-center">
			<?php
				$section1 = get_field('gco_section_1');
				if( $section1 ): ?>
				<div class="blocImg">
					<div class="imgBig wow fadeInUp">
						<img src="<?= $section1['image_2']; ?>" alt="">
					</div>
					<div class="imgSmall wow slideInLeft" data-wow-duration="1s" data-wow-delay="1s">
						<img src="<?= $section1['image_1']; ?>" alt="">
					</div>
					<div class="imgVerySmall wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay="1.2s">
						<img src="<?= $section1['image_3']; ?>" alt="">
					</div>
				</div>
				<div class="blocText wow fadeInRight">
				    <div class="blocTitre">
                        <span><?= $section1['titre_1']; ?></span>
                        <h2><?= $section1['titre_2']; ?></h2>
					</div>
                    <p><?= $section1['description']; ?></p>
                </div>
			<?php endif; ?>
			</div>
		</section>
				
        <section id="section2" class="blocInterSlog d-flex justify-content-center align-items-center">
            <div class="container">			
				<?php $section21 = get_field('gco_section_21');
				if( $section21 ): ?>
					<div class="row introText">
						<div class="col text-center wow fadeInUp" data-wow-duration="600ms" data-wow-delay="600ms">
							<p><?= $section21 ?></p>
						</div>
					</div>
				<?php endif; ?>
            </div>
        </section>
		<section id="section16" class="d-flex justify-content-center align-items-center">
			<div class="container d-flex justify-content-between align-items-center">
				<?php
					$section2 = get_field('gco_section_2');
					if( $section2 ): ?>
					<div class="blocText wow fadeInLeft">
						<div class="blocTitre">
							<span><?= $section2['titre_1']; ?></span>
							<h2><?= $section2['titre_2']; ?></h2>
						</div>
						<?= $section2['description']; ?>
					</div>
					<div class="blocImg">
						<div class="imgBig wow fadeInUp">
							<img src="<?= $section2['image_2']; ?>" alt="">
						</div>
						<div class="imgSmall wow slideInRight" data-wow-duration="1s" data-wow-delay="1s">
							<img src="<?= $section2['image_1']; ?>" alt="">
						</div>
						<div class="imgVerySmall wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="1.2s">
							<img src="<?= $section2['image_3']; ?>" alt="">
						</div>
					</div>
				<?php endif; ?>
            </div>
		</section>
        <section id="section17" class="d-flex justify-content-center align-items-center">
			<div class="container d-flex justify-content-center align-items-center">
				<?php
				$section3 = get_field('gco_section_3');
				if( $section3 ): ?>
					<div class="blocImg">
						<div class="imgBig wow fadeInUp">
							<img src="<?= $section3['image_1']; ?>" alt="">
						</div>
						<div class="imgSmall wow slideInLeft" data-wow-duration="1s" data-wow-delay="1s">
							<img src="<?= $section3['image_2']; ?>" alt="">
						</div>
					</div>
					<div class="blocText wow fadeInRight">
						<div class="blocTitre">
							<span><?= $section3['titre_1']; ?></span>
							<h2><?= $section3['titre_2']; ?></h2>
						</div>
						<p><?= $section3['description']; ?></p>
					</div>
				<?php endif; ?>
			</div>
		</section>
    </main>

<?php

	get_footer();
?>