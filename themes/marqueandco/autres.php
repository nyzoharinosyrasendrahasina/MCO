<?php
/*
  Template Name: Homepage
*/
  	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
	}

	get_header();
?>
<main>       
		<section id="section11">
            <div class="container">
                <div class="row d-flex justify-content-center align-items-center">
					<div class="img col-md-6 wow fadeInLeft">
                        <img src="<?= IMG_URL."img-projet.jpg"?>" alt="">
                    </div>
					<div class="text col-md-6 wow fadeInLeft">
						<div class="blocTitre">
							<span>Projets Radio Alfa</span>
							<h2>Nouveau visage <br>d’une Radio</h2>
						</div>
						<p>Refonte de l’identité visuelle, Brand Content, Social Média, Print, Site Web, Presse & Event</p>
					</div>
                </div>
            </div>
        </section>
		<section id="section12">
			<div class="container">
				<div class="row">
					<img src="<?= IMG_URL."detail-projet2.jpg" ?>" alt="">
				</div>
			</div>
		</section>
		<section id="section13">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<img src="<?= IMG_URL."detail-projet3.jpg" ?>" alt="">
					</div>
					<div class="col-md-6 col-sm-12">
						<img src="<?= IMG_URL."detail-projet4.jpg" ?>" alt="">
					</div>
				</div>
			</div>
		</section>
		<section id="section14">
			<div class="container">
				<div class="row">
					<img src="<?= IMG_URL."detail-projet5.jpg" ?>" alt="">
				</div>
			</div>
		</section>
		<section id="section15">
			<div class="container">
				<div class="row">
					<img src="<?= IMG_URL."detail-projet6.jpg" ?>" alt="">
				</div>
			</div>
		</section>
    </main>

<?php

	get_footer();
?>